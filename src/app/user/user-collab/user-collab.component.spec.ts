import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCollabComponent } from './user-collab.component';

describe('UserCollabComponent', () => {
  let component: UserCollabComponent;
  let fixture: ComponentFixture<UserCollabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserCollabComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserCollabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
