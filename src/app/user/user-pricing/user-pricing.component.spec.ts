import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPricingComponent } from './user-pricing.component';

describe('UserPricingComponent', () => {
  let component: UserPricingComponent;
  let fixture: ComponentFixture<UserPricingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPricingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
