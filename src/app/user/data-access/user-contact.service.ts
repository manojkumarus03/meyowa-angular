import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/model/user';

@Injectable({
  providedIn: 'root'
})
export class UserContactService {

  private readonly BASE_URL = 'http://localhost:8080/user';

  constructor(private http: HttpClient) { }

  saveUser(newUser: User): Observable<Object> {
    console.log(newUser);
    return this.http.post(this.BASE_URL, newUser);
  }

}
