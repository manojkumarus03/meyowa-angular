import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { User } from 'src/app/shared/model/user';
import { UserContactService } from '../data-access/user-contact.service';

@Component({
  selector: 'app-user-contact',
  templateUrl: './user-contact.component.html',
  styleUrls: ['./user-contact.component.scss']
})
export class UserContactComponent implements OnInit {

  user: User | null = null;
  contactUsForm!: FormGroup;
  @ViewChild("formGroupDirective") private formGroupDirective!: FormGroupDirective;
  userInfoSaved = false;

  constructor(private fb: FormBuilder, private userContactService: UserContactService) { }

  ngOnInit(): void {
    this.contactUsForm = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      userEmail: ['', [Validators.required, Validators.pattern("^[a-z0-9](\.?[a-z0-9]){2,}@(gmail|yahoo|google)\.com$")]],
      userComment: ['']
    });
  }

  get getUserName() {
    return this.contactUsForm.get('userName');
  }

  get getUserEmail() {
    return this.contactUsForm.get('userEmail');
  }

  saveUserInfo(): void {
    if (!this.contactUsForm.valid) {
      this.contactUsForm.markAllAsTouched();
      return;
    }
    this.user = this.contactUsForm.value as User;
    this.userContactService.saveUser(this.user).subscribe({
      next: data => {
        alert("message send successfully!");
        this.formGroupDirective.resetForm();
      },
      error: error => alert("something went wrong!")
    });
  }

}
