import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentAlertsComponent } from './component/component-alerts/component-alerts.component';
import { ComponentBadgesComponent } from './component/component-badges/component-badges.component';
import { ComponentButtonsComponent } from './component/component-buttons/component-buttons.component';
import { ComponentDropdownsComponent } from './component/component-dropdowns/component-dropdowns.component';
import { ComponentInputsComponent } from './component/component-inputs/component-inputs.component';
import { ComponentNavigationsComponent } from './component/component-navigations/component-navigations.component';
import { ComponentPaginationsComponent } from './component/component-paginations/component-paginations.component';
import { ComponentProgressbarsComponent } from './component/component-progressbars/component-progressbars.component';
import { ComponentTablesComponent } from './component/component-tables/component-tables.component';
import { ComponentTypographyComponent } from './component/component-typography/component-typography.component';
import { ComponentComponent } from './component/component.component';
import { FooterComponent } from './shared/footer/footer.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { UserAboutComponent } from './user/user-about/user-about.component';
import { UserBlogComponent } from './user/user-blog/user-blog.component';
import { UserCollabComponent } from './user/user-collab/user-collab.component';
import { UserContactComponent } from './user/user-contact/user-contact.component';
import { UserIntroComponent } from './user/user-intro/user-intro.component';
import { UserPortfolioComponent } from './user/user-portfolio/user-portfolio.component';
import { UserPricingComponent } from './user/user-pricing/user-pricing.component';
import { UserServiceComponent } from './user/user-service/user-service.component';
import { UserTestimonialComponent } from './user/user-testimonial/user-testimonial.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavbarComponent,
    UserComponent,
    UserIntroComponent,
    UserAboutComponent,
    UserServiceComponent,
    UserPortfolioComponent,
    UserPricingComponent,
    UserCollabComponent,
    UserTestimonialComponent,
    UserBlogComponent,
    UserContactComponent,
    ComponentComponent,
    ComponentButtonsComponent,
    ComponentInputsComponent,
    ComponentDropdownsComponent,
    ComponentPaginationsComponent,
    ComponentBadgesComponent,
    ComponentAlertsComponent,
    ComponentTablesComponent,
    ComponentTypographyComponent,
    ComponentNavigationsComponent,
    ComponentProgressbarsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
