export interface User {
    userName: string,
    userEmail: string,
    userComment: string
}
