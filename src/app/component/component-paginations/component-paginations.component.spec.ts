import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentPaginationsComponent } from './component-paginations.component';

describe('ComponentPaginationsComponent', () => {
  let component: ComponentPaginationsComponent;
  let fixture: ComponentFixture<ComponentPaginationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentPaginationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponentPaginationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
