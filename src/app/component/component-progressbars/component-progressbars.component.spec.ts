import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentProgressbarsComponent } from './component-progressbars.component';

describe('ComponentProgressbarsComponent', () => {
  let component: ComponentProgressbarsComponent;
  let fixture: ComponentFixture<ComponentProgressbarsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentProgressbarsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponentProgressbarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
