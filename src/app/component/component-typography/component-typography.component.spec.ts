import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentTypographyComponent } from './component-typography.component';

describe('ComponentTypographyComponent', () => {
  let component: ComponentTypographyComponent;
  let fixture: ComponentFixture<ComponentTypographyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentTypographyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponentTypographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
