import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentNavigationsComponent } from './component-navigations.component';

describe('ComponentNavigationsComponent', () => {
  let component: ComponentNavigationsComponent;
  let fixture: ComponentFixture<ComponentNavigationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentNavigationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponentNavigationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
