import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentInputsComponent } from './component-inputs.component';

describe('ComponentInputsComponent', () => {
  let component: ComponentInputsComponent;
  let fixture: ComponentFixture<ComponentInputsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentInputsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponentInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
